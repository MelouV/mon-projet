# Ceci est le contenu du fichier README.md 
# Foobar devient Mon projet

Mon projet is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```
## Commit 1 de l'exercice p62

## Commit 2 de l'exercice p62

## Usage

```python
import foobar

# returns 'words'
foobar.pluralize('local + server')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
foobar.singularize('local + server')
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
